<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Laravel</title>
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link rel="stylesheet" href="{{asset('css/myApp.css')}}">
  </head>
  <body>
    <div class="container">
      <h2>Let's Consult</h2><br/>
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                        FullStack Developer : <a target="_tab" href="http://notes.sk/zadanie/public/">ZADANIE</a> 
                        </div>
                        <div class="panel-body">
                        
                        @if ($errors->any())
                          <div class="alert alert-danger alert-dismissible">
                              <ul>
                                  @foreach ($errors->all() as $error)
                                      <li>{{ $error }}</li>
                                  @endforeach
                              </ul>
                          </div><br />
                          @endif

                          {{--  FORM  --}}
                          {{csrf_field()}}
                          <form method="post" action="/">
                          <div class="row">
                            <div class="col-md-4"></div>
                              <input id="checker" type="checkbox" for="sizeServerRack" name="rndRack"> náhodne obsaď pozície v Racku<br><br>
                          </div>
                           <div class="row">
                            <div class="col-md-4"></div>
                              <div class="form-group col-md-4">
                                <label for="sizeServerRack">Veľkosť serverového Racku:</label>
                              <input type="text" class="form-control" id="sizeServerRack" name="sizeServerRack" value="8">
                            </div>
                          </div>
                          <hr>
                          <div class="row">
                            <div class="col-md-4"></div>
                              <div class="form-group col-md-4">
                                <label for="sizeRack">Veľkost zariadenia:</label>
                              <input type="text" class="form-control" name="sizeRack">
                            </div>
                          </div>
                          <div class="row">
                            <div class="col-md-4"></div>
                            <div class="form-group col-md-4">
                              <button type="submit" class="btn btn-success">Pridaj zariadenie</button>
                            </div>
                          </div>
                        </form>
                          @if (\Session::has('success'))
                          <div class="alert alert-success alert-dismissible center">
                              <p>{{ \Session::get('success') }}</p>
                          </div><br />
                          @endif
                          {{--  Sekcia pre zobrazenie vysledku  --}}
                          @isset($countPositions)
                          <div class="row">
                            <div class="col-md-6">
                                <h3 class="center" >Serverový Rack</h3>
                                <div class="well">
                                    $positions = [<br>
                                    @foreach ($positions as $key => $value) 
                                    <div style="padding-left: 40px;">
                                        {{ $key . ' => [ id = ' . $value['id'] . ', occupied = ' . $value['occupied'] . ' ],' }}<br>
                                    </div>
                                    @endforeach
                                    ]
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h3 class="center" >Výsledok</h3>
                                <div class="well">
                                @if ($countPositions > 0)
                                  Počet pozícii na umiestnenie zariadenia : <strong>{{ $countPositions }}</strong>
                                  <hr>
                                  @foreach ($passedPositions as $value) 
                                      ID Rack pozície : {{ $value }}<br>
                                  @endforeach
                                @else
                                  Zariadenie nie je možné umiestniť na žiadnu pozíciu :( .
                                  <br><br>
                                  <div class="row">
                                    <div class="content-media">
                                      <div class="post-thumb">
                                        <img src="{{ asset('img/Full house.jpg')}}">
                                      </div>
                                    </div>
                                  </div>
                                  <div class="center"><strong>Full House :D</strong></div>
                                @endif
                                </div>
                            </div>
                          </div>
                          @endisset
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/myApp.js') }}"></script>
  </body>
</html>