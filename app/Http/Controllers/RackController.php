<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Validator;
use Illuminate\Support\Facades\Session;

class RackController extends Controller
{
    public function index()
    {
        return view('homeworks.zadanie');
    }

    public function store(Request $request)
    {
        if (isset($_POST['rndRack'])) {
            // Checkbox is selected
            $this->validate(request(),[
                'sizeServerRack' => 'required|numeric|min:1|max:100',
                'sizeRack' => 'required|numeric|min:1|max:100'
            ]);
        } else {
            $this->validate(request(),[
                'sizeRack' => 'required|numeric|min:1|max:100'
            ]);
        }

        $sizeServerRack = $request->get('sizeServerRack');
        $sizeRack =$request->get('sizeRack');

        // TODO - dorobit validaciu na realne cisla
        
        // Init array 
        $positions = []; 
        if ($sizeServerRack > 0) { 
            for ($i = 0; $i < $sizeServerRack; $i++) { 
                // $positions[] = [$i => ['id' => $i+1, 'occupied' => rand(0,1)]]; 
                $positions[] = ['id' => $i+1, 'occupied' => rand(0,1)];                 
            } 
        } 
        else { 
            $positions = [ 
                0 => ['id' => 1, 'occupied' => 0], 
                1 => ['id' => 2, 'occupied' => 1], 
                2 => ['id' => 3, 'occupied' => 1], 
                3 => ['id' => 4, 'occupied' => 0], 
                4 => ['id' => 5, 'occupied' => 0], 
                5 => ['id' => 6, 'occupied' => 0], 
                6 => ['id' => 7, 'occupied' => 0], 
                7 => ['id' => 8, 'occupied' => 0], 
            ]; 
        } 

        // search position in Rack
        $passedPositions=[];

        reset($positions); 
        for ($i=0; $i<count($positions)-($sizeRack-1); $i++) {  
            if ($positions[$i]['occupied'] == 0) { 
                $id = $positions[$i]['id'];
                $freeRack=1;
        
                // cyklus pre overenie volne po sebe iducich pozicii 
                for ($j=1; $j<$sizeRack; $j++) { 
                    if ($positions[$i+$j]['occupied'] == 1 || $freeRack == $sizeRack) { 
                        break; 
                    } else{
                        $freeRack++;
                    }
                }
                if ($freeRack == $sizeRack){
                    $passedPositions[] = $id; 
                }
            }
        }
        $countPositions = sizeof($passedPositions);

        
        Session::flash('success', 'Vstupné paramatre boli úspešne prijaté a spracované.');
        
        return view('homeworks.zadanie', compact('countPositions','passedPositions','positions'))
            ->with('success','Vstupné paramatre boli úspešne prijaté a spracované.') ;
        // return back()->with('passedPositions',$passedPositions)
        //         ->with('countPositions',$countPositions);    
        //         ->with('success','Vstupné paramatre boli úspešne prijaté a spracované.')   ; 
        // return View::make('reports.international-calls')->with('data', $data);
    }
}